mod config;
mod input;
#[macro_use]
extern crate clap;
use clap::{App, ArgMatches};

use hex;
use home;
use indicatif::{MultiProgress, ProgressBar, ProgressStyle};
use input::{PassphraseInputPrompt, PinentryPassphraseInput, TerminalPassphraseInput};
use libcryptocam::{
    decrypt::{self, ProgressCallback},
    key_qrcode::make_qr_code,
    keyring::{self, KeyDigest, Keyring},
};
use libcryptocam::{keyring::DisplayIdentity, qrcode::render::unicode};
use log::{error, info, warn};
use simple_logger::SimpleLogger;
use std::{
    cmp,
    convert::TryInto,
    fs::{self, File},
    path::PathBuf,
    sync::{atomic::AtomicBool, Arc},
};
use threadpool::ThreadPool;

fn main() {
    let yaml = load_yaml!("cli.yml");
    let matches = App::from(yaml)
        .setting(clap::AppSettings::SubcommandRequiredElseHelp)
        .get_matches();
    SimpleLogger::new()
        .with_module_level("pinentry", log::LevelFilter::Error)
        .with_level(log::LevelFilter::Info)
        .init()
        .unwrap();

    if matches.subcommand().is_none() {
        return;
    }

    match matches.subcommand() {
        Some(("set-default-keyring", subcommand)) => {
            if let Some(path) = subcommand.value_of_os("PATH") {
                let path = PathBuf::from(path);
                if !path.exists() {
                    match fs::create_dir(&path) {
                        Ok(()) => {}
                        Err(e) => {
                            error!("Error creating directory {}: {}", path.to_string_lossy(), e);
                            std::process::exit(1);
                        }
                    }
                }
                let path = match fs::canonicalize(&path) {
                    Ok(p) => p,
                    Err(e) => {
                        error!(
                            "Error getting canonical path for {}: {}",
                            path.to_string_lossy(),
                            e
                        );
                        std::process::exit(1);
                    }
                };
                if let Err(e) = config::set_keyring_path(&path) {
                    error!("Error writing to config file: {}", e);
                    std::process::exit(1);
                }
            }
            return;
        }
        _ => {}
    };

    let keyring_path: PathBuf = if let Some(k) = matches.value_of("keyring") {
        k.into()
    } else {
        let path = match config::keyring_path_from_config() {
            Some(path) => path,
            None => config::default_keyring(),
        };
        if !path.exists() {
            let create_keyring = match dialoguer::Confirm::new()
                .with_prompt(format!("Create keyring in {}?", path.to_string_lossy()))
                .interact()
            {
                Err(e) => {
                    error!("Could not create confirmation prompt: {}", e);
                    std::process::exit(1);
                }
                Ok(b) => b,
            };
            if create_keyring {
                match fs::create_dir(&path) {
                    Err(e) => {
                        error!(
                            "Could not create directory {}: {}",
                            path.to_string_lossy(),
                            e
                        );
                        std::process::exit(1);
                    }
                    Ok(_) => {}
                };
            } else {
                std::process::exit(1);
            }
        }
        path
    };
    let mut keyring = match Keyring::load_from_directory(keyring_path) {
        Err(e) => {
            error!("Could not load keyring: {}", e);
            std::process::exit(1);
        }
        Ok(k) => k,
    };
    match matches.subcommand() {
        None => {}
        Some(("list-keys", _)) => {
            eprintln!("{0: <16} {1: <32} {2: <63}", "Digest", "Name", "Public key");
            for identity in keyring.display_identities() {
                println!(
                    "{0: <16} {1: <32} {2: <63}",
                    hex::encode(identity.public_key_digest),
                    identity.name,
                    identity.public_key
                );
            }
        }
        Some(("key-gen", subcommand)) => gen_key(subcommand, &mut keyring),
        Some(("decrypt", subcommand)) => decrypt(subcommand, &mut keyring),
        Some(("qrcode", subcommand)) => qrcode(subcommand, &keyring),
        Some(("delete-key", subcommand)) => {
            unimplemented!()
        }
        _ => {
            unimplemented!()
        }
    };
}

fn gen_key(subcommand: &ArgMatches, keyring: &mut Keyring) {
    if let Some(key_name) = subcommand.value_of_os("key_name") {
        let description = String::from("Enter a passphrase to encrypt the new key.");
        let prompt = String::from("Passphrase");
        let confirm = String::from("Confirm passphrase");
        let passphrase = if input::pinentry_available() {
            let mut i = PinentryPassphraseInput::new();
            i.with_description(description);
            i.with_prompt(prompt);
            i.with_confirm(confirm);
            i.read_secret()
        } else {
            let mut i = TerminalPassphraseInput::new();
            i.with_description(description);
            i.with_prompt(prompt);
            i.with_confirm(confirm);
            i.read_secret()
        };
        let passphrase = match passphrase {
            Err(e) => {
                error!("{}", e);
                std::process::exit(1);
            }
            Ok(p) => {
                if p.is_empty() {
                    warn!("Empty passphrase. Your secret key will be stored unencrypted.");
                    None
                } else {
                    Some(p)
                }
            }
        };
        match keyring.create_key(&key_name.to_string_lossy(), passphrase.as_deref()) {
            Err(e) => {
                error!("Error generating key: {}", e);
                std::process::exit(1);
            }
            Ok(identity) => {
                println!("Key digest: {}", hex::encode(identity.public_key_digest));
                println!("Public key: {}", identity.public_key);
                print_qrcode(&identity);
            }
        }
    }
}

struct BarProgressCallback {
    total_file_size: u64,
    offset: u64,
    filename: String,
    bar: ProgressBar,
}

impl BarProgressCallback {
    pub fn new(bar: ProgressBar, filename: String) -> BarProgressCallback {
        BarProgressCallback {
            bar,
            total_file_size: 0,
            offset: 0,
            filename,
        }
    }
}

impl ProgressCallback for BarProgressCallback {
    fn set_offset(&mut self, offset: u64) {
        self.offset = offset;
    }

    fn set_total_file_size(&mut self, n: u64) {
        self.total_file_size = n;
    }

    fn on_progress(&mut self, processed_bytes: u64) {
        self.bar
            .set_position(100 * (self.offset + processed_bytes) / self.total_file_size);
    }

    fn on_complete(&mut self) {
        self.bar
            .finish_with_message(format!("{} DONE", self.filename).as_str());
    }

    fn on_error(&mut self, error: Box<dyn std::error::Error>) {
        self.bar
            .set_message(&format!("{} ERROR: {}", self.filename, error));
        self.bar.finish_at_current_pos();
    }
}

fn decrypt(subcommand: &ArgMatches, keyring: &mut Keyring) {
    let input_paths = subcommand.values_of_os("FILES").unwrap().to_owned();
    let input_files: Vec<PathBuf> = input_paths
        .map(|p| PathBuf::from(p))
        .filter(|p| p.exists())
        .collect();
    let out_path = subcommand
        .value_of_os("output_dir")
        .map(|p| {
            let path = PathBuf::from(p);
            if !path.exists() {
                match fs::create_dir(&path) {
                    Err(e) => {
                        error!(
                            "Error creating directory {}: {}",
                            &path.to_string_lossy(),
                            e
                        );
                        std::process::exit(1);
                    }
                    Ok(_) => {}
                };
            }
            path
        })
        .unwrap_or(PathBuf::new());
    let multi_progress = MultiProgress::new();
    let style = ProgressStyle::default_bar()
        .template("[{elapsed_precise}] {bar:40.green/white} {percent}% {msg}");
    let mut jobs = Vec::new();
    for file_path in input_files {
        let filename = file_path.file_name().unwrap().to_string_lossy().to_string();
        let progress_bar = multi_progress.add(ProgressBar::new(100));
        progress_bar.set_style(style.clone());
        progress_bar.set_message(&filename);
        let progress_callback = BarProgressCallback::new(progress_bar, filename);
        let file_path = file_path.to_string_lossy().to_string();
        let use_pinentry = input::pinentry_available();

        let mut retries = 3;
        let mut display_error: Option<String> = None;
        let mut input = if use_pinentry {
            Box::new(PinentryPassphraseInput::new()) as Box<dyn PassphraseInputPrompt>
        } else {
            Box::new(TerminalPassphraseInput::new()) as Box<dyn PassphraseInputPrompt>
        };
        while retries > 0 {
            // yes, we're reopening the file on every retry here because getting back ownership
            // from decrypt(...) makes everything 10 times as complicated than it needs to be
            let file = match File::open(&file_path) {
                Err(e) => {
                    error!("Could not open {}: {}", file_path, e);
                    std::process::exit(1);
                }
                Ok(f) => f,
            };
            match decrypt::decrypt(file, keyring, out_path.clone()) {
                Err(error) => match error.downcast::<keyring::DecryptionError>() {
                    Ok(error) => match error {
                        keyring::DecryptionError::IdentityEncrypted(identity) => {
                            input.with_description(format!(
                                "Enter passphrase for key '{}'",
                                identity.name
                            ));
                            input.with_prompt("Passphrase".to_string());
                            if let Some(error) = &display_error {
                                input.with_error(error.clone());
                            }
                            let passphrase = match input.read_secret() {
                                Err(e) => {
                                    error!("Could not get passphrase: {}", e);
                                    std::process::exit(1);
                                }
                                Ok(p) => p,
                            };
                            match keyring.decrypt_identity(&identity.public_key_digest, passphrase)
                            {
                                Err(error) => match error {
                                    keyring::DecryptIdentityError::WrongPassphrase => {
                                        display_error = Some(error.to_string());
                                        retries -= 1;
                                        continue;
                                    }
                                    other => {
                                        error!("Could not decrypt identity: {}", other);
                                        std::process::exit(1);
                                    }
                                },
                                Ok(()) => continue,
                            };
                        }
                        other => {
                            eprintln!("Warning: could not decrypt {}: {}", file_path, other);
                            break;
                        }
                    },
                    Err(error) => {
                        eprintln!("Warning: could not decrypt {}: {}", file_path, error);
                        break;
                    }
                },
                Ok(decrypting_job) => {
                    jobs.push((decrypting_job, progress_callback));
                    break;
                }
            }
        }
    }
    if jobs.len() < 1 {
        std::process::exit(1);
    }
    let num_threads: usize = cmp::min(8, jobs.len());
    let pool = ThreadPool::new(num_threads);
    for (mut decrypting_job, mut progress_callback) in jobs {
        pool.execute(move || {
            decrypting_job.run(
                Box::new(&mut progress_callback),
                Arc::new(AtomicBool::new(false)),
            )
        });
    }
    multi_progress.join().unwrap()
}

fn qrcode(subcommand: &ArgMatches, keyring: &Keyring) {
    if let Some(key_digest_arg) = subcommand.value_of_os("KEY") {
        match hex::decode(key_digest_arg.to_string_lossy().to_string()) {
            Err(_) => {
                error!("Invalid key fingerprint");
                std::process::exit(1)
            }
            Ok(bytes) => {
                let key_digest: KeyDigest = match bytes.as_slice().try_into() {
                    Err(_) => {
                        error!("Invalid key fingerprint");
                        std::process::exit(1)
                    }
                    Ok(kd) => kd,
                };
                let identity = match keyring.get_identity(&key_digest) {
                    Err(e) => {
                        error!("Error: {}", e);
                        std::process::exit(1)
                    }
                    Ok(i) => i,
                };
                print_qrcode(&identity);
            }
        }
    }
}

fn print_qrcode(identity: &DisplayIdentity) {
    match make_qr_code(&identity) {
        Err(e) => {
            error!("Error: {}", e);
            std::process::exit(1)
        }
        Ok(code) => {
            let image = code
                .render::<unicode::Dense1x2>()
                .light_color(unicode::Dense1x2::Light)
                .dark_color(unicode::Dense1x2::Dark)
                .build();
            println!("{}", image);
        }
    };
}
